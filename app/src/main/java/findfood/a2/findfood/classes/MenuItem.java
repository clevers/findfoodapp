package findfood.a2.findfood.classes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by amanda on 22/07/17.
 */

public class MenuItem {

    private String menuUrl;
    private String name;
    private String category;
    private String description;
    private double price;
    private String restaurantURL;
    private Restaurant restaurant;
    private boolean mylike;
    private boolean mydislike;
    private String likeURL;
    private double distance;
    private String restaurantName;

    public MenuItem(String menuUrl, String name, String category,
                    String description, double price, String restaurantURL,
                    boolean mylike, boolean mydislike, String likeURL, double distance, String restaurantName) throws JSONException{

        this.menuUrl = menuUrl;
        this.name = name;
        this.category = category;
        this.description = description;
        this.price = price;
        this.restaurantURL = restaurantURL;
        this.restaurant = null;
        if(restaurantURL != null && restaurantURL.startsWith("{")){
            this.restaurant = new Restaurant(new JSONObject(restaurantURL));
        }
        this.mylike = mylike;
        this.mydislike = mydislike;
        this.likeURL = likeURL;
        this.distance = distance;
        this.restaurantName = restaurantName;
    }

    public MenuItem(JSONObject obj) throws JSONException{
        this(obj.getString("id"),
                obj.getString("name"),
                obj.isNull("category") ? null : obj.getString("category"),
                obj.isNull("description") ? null : obj.getString("description"),
                obj.isNull("price") ? -1 : obj.getDouble("price"),
                obj.isNull("restaurant") ? null : obj.getString("restaurant"),
                !obj.isNull("mylike") && obj.getBoolean("mylike"),
                !obj.isNull("mylike") && !obj.getBoolean("mylike"),
                obj.isNull("likeURL") ? null : obj.getString("likeURL"),
                obj.isNull("distance") ? -1 : obj.getDouble("distance"),
                obj.isNull("restaurantName") ? null : obj.getString("restaurantName"));
    }

    public String getMylikeJSON() throws JSONException {
        JSONObject out = new JSONObject();
        if(mylike && !mydislike){
            out.put("like", true);
        }
        else if( !mylike && mydislike){
            out.put("like", false);
        }
        else{
            out.put("like", JSONObject.NULL);
        }
        return out.toString();
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean hasPrice(){
        return (price  != 0 && price != -1);
    }

    public double getPrice() {
        return price;
    }

    public String getRestaurantURL() {
        return restaurantURL;
    }

    public String getCategory() {
        return category;
    }

    public boolean isMylike() {
        return mylike;
    }

    public boolean isMydislike() {
        return mydislike;
    }

    public void setMylike(boolean mylike) {
        this.mylike = mylike;
    }

    public void setMydislike(boolean mydislike) {
        this.mydislike = mydislike;
    }

    public String getLikeURL() {
        return likeURL;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public boolean hasDistance(){
        if (distance == -1){
            return false;
        }
        return true;
    }

    public double getDistance() {
        return distance;
    }

    public String getRestaurantName() {
        return restaurantName;
    }
}

