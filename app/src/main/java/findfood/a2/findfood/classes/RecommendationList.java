package findfood.a2.findfood.classes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by aluizio on 14/09/17.
 */

public class RecommendationList {
    private ArrayList<RecommendatedItem> recommendations;
    private String nextUrl;
    private int total;

    public RecommendationList(JSONArray menuItemJson, String nextUrl, int total) throws JSONException {

        recommendations = new ArrayList<>();
        this.total = total;
        updateList(menuItemJson, nextUrl);
    }

    public RecommendationList(JSONObject obj) throws JSONException {
        this(   obj.getJSONArray("results"),
                obj.isNull("next") ? null : obj.get("next").toString(),
                Integer.parseInt(obj.get("count").toString()));
    }

    public void updateList(JSONObject obj) throws JSONException {
        updateList(
                obj.getJSONArray("results"),
                obj.isNull("next") ? null : obj.get("next").toString()
        );
    }

    public void updateList(JSONArray recomendatedItemJson, String nextUrl) throws JSONException {
        this.nextUrl = nextUrl;
        for (int i = 0 ; i < recomendatedItemJson.length(); i++) {
            JSONObject obj = recomendatedItemJson.getJSONObject(i);
            RecommendatedItem recomendatedItem = new RecommendatedItem(obj);
            recommendations.add(recomendatedItem);
        }
    }

    public int size(){
        return recommendations.size();
    }

    public ArrayList<RecommendatedItem> getRecommendations() {
        return recommendations;
    }

    public String getNextUrl() {
        return nextUrl;
    }

    public int getTotal() {
        return total;
    }
}
