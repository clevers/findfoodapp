package findfood.a2.findfood.classes;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import findfood.a2.findfood.R;

/**
 * Created by aluizio on 11/09/17.
 */

public class MenuItemHolder extends RecyclerView.ViewHolder {

    final TextView name;
    final TextView category;
    final TextView distance;
    final TextView restaurantName;

    public MenuItemHolder(View itemView) {
        super(itemView);
        name = (TextView) itemView.findViewById(R.id.list_menu_item_name);
        category = (TextView) itemView.findViewById(R.id.list_menu_item_category);
        distance = (TextView) itemView.findViewById(R.id.list_menu_item_distance);
        restaurantName = (TextView) itemView.findViewById(R.id.list_menu_item_restaurant);
    }
}
