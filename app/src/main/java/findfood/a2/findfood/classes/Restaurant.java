package findfood.a2.findfood.classes;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by aluizio on 16/09/17.
 */

public class Restaurant {
    private String restaurantUrl;
    private String name;
    private String description;
    private String servesCuisine;
    private String telephone;
    private String url;
    private String menu;
    private double latitude;
    private double longitude;


    public Restaurant(String restaurantUrl, String name, String description, String servesCuisine, String telephone, String url, String menu, double latitude, double longitude) {
        this.restaurantUrl = restaurantUrl;
        this.name = name;
        this.description = description;
        this.servesCuisine = servesCuisine;
        this.telephone = telephone;
        this.url = url;
        this.menu = menu;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Restaurant(JSONObject obj) throws JSONException{
        this(   obj.getString("id"),
                obj.getString("name"),
                obj.isNull("description") ? null : obj.getString("description"),
                obj.isNull("servesCuisine") ? null : obj.getString("servesCuisine"),
                obj.isNull("telephone") ? null : obj.getString("telephone"),
                obj.getString("url"),
                obj.getString("menu"),
                obj.getJSONObject("geo").getDouble("latitude"),
                obj.getJSONObject("geo").getDouble("longitude"));
    }

    public String getRestaurantUrl() {
        return restaurantUrl;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getServesCuisine() {
        return servesCuisine;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getUrl() {
        return url;
    }

    public String getMenu() {
        return menu;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
