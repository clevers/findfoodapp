package findfood.a2.findfood.classes;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import findfood.a2.findfood.R;

/**
 * Created by aluizio on 11/09/17.
 */

public class MenuItemAdapter extends RecyclerView.Adapter {

    private MenuItemList menuItemList;
    private Context context;
    private boolean showRestaurantName;

    public MenuItemAdapter(MenuItemList menuItemList, Context context){
        this(menuItemList, context, true);
    }

    public MenuItemAdapter (MenuItemList menuItemList, Context context, boolean showRestaurantName){
        this.menuItemList = menuItemList;
        this.context = context;
        this.showRestaurantName = showRestaurantName;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.menuitem_layout, parent, false);
        MenuItemHolder menuItemViewHolder = new MenuItemHolder(view);
        return menuItemViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MenuItemHolder menuItemHolder = (MenuItemHolder) holder;
        MenuItem menuItem = menuItemList.getMenuItems().get(position);

        menuItemHolder.name.setText(menuItem.getName());
        if(showRestaurantName)
            menuItemHolder.restaurantName.setText(menuItem.getRestaurantName());

        String category = menuItem.getCategory();
        if( category != null)
            menuItemHolder.category.setText(category);
        if(menuItem.hasDistance()) {
            int d = (int) menuItem.getDistance();
            menuItemHolder.distance.setText(d + " m");
        }
    }

    @Override
    public int getItemCount() {
        return menuItemList.size();
    }
}
