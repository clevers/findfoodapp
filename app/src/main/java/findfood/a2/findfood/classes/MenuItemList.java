package findfood.a2.findfood.classes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class MenuItemList {
    private ArrayList<MenuItem> menuItems;
    private String nextUrl;
    private int total;

    public MenuItemList(JSONArray menuItemJson, String nextUrl, int total) throws JSONException {

        menuItems = new ArrayList<>();
        this.total = total;
        updateList(menuItemJson, nextUrl);
    }

    public MenuItemList(JSONObject obj) throws JSONException {
        this(   obj.getJSONArray("results"),
                obj.isNull("next") ? null : obj.get("next").toString(),
                Integer.parseInt(obj.get("count").toString()));
    }

    public void updateList(JSONObject obj) throws JSONException {
        updateList(
                obj.getJSONArray("results"),
                obj.isNull("next") ? null : obj.get("next").toString()
        );
    }

    public void updateList(JSONArray menuItemJson, String nextUrl) throws JSONException {
        this.nextUrl = nextUrl;
        for (int i = 0 ; i < menuItemJson.length(); i++) {
            JSONObject obj = menuItemJson.getJSONObject(i);
            MenuItem menuItem = new MenuItem(obj);
            menuItems.add(menuItem);
        }
    }

    public int size(){
        return menuItems.size();
    }

    public ArrayList<MenuItem> getMenuItems() {
        return menuItems;
    }

    public String getNextUrl() {
        return nextUrl;
    }
}
