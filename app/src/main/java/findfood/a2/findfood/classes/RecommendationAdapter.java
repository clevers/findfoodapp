package findfood.a2.findfood.classes;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import findfood.a2.findfood.R;

/**
 * Created by aluizio on 14/09/17.
 */

public class RecommendationAdapter extends RecyclerView.Adapter {
    private RecommendationList recomendations;
    private Context context;

    public RecommendationAdapter(RecommendationList recomendationList, Context context){
        this.recomendations = recomendationList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.recommendation_layout, parent, false);
        MenuItemHolder menuItemViewHolder = new MenuItemHolder(view);
        return menuItemViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MenuItemHolder menuItemHolder = (MenuItemHolder) holder;
        MenuItem menuItem = recomendations.getRecommendations().get(position).getMenuItem();

        menuItemHolder.name.setText(menuItem.getName());
        menuItemHolder.restaurantName.setText(menuItem.getRestaurantName());
        String category = menuItem.getCategory();
        if( category != null)
            menuItemHolder.category.setText(category);
    }

    @Override
    public int getItemCount() {
        return recomendations.size();
    }
}
