package findfood.a2.findfood.classes;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by aluizio on 14/09/17.
 */

public class RecommendatedItem {
    private double relevancy;
    private MenuItem menuItem;

    public RecommendatedItem(JSONObject obj) throws JSONException {
        this(obj.getDouble("relevancy"),
                new MenuItem(obj.getJSONObject("menuitem")));
    }

    public RecommendatedItem(double relevancy, MenuItem menuItem){
        this.relevancy = relevancy;
        this.menuItem = menuItem;
    }

    public double getRelevancy() {
        return relevancy;
    }

    public MenuItem getMenuItem() {
        return menuItem;
    }
}
