package findfood.a2.findfood.classes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Map;

/**
 * Created by amanda on 14/07/17.
 */

public class NetworkUtils {

    public final static String get = "GET";
    public final static String post = "POST";
    public final static String put = "PUT";
    public final static String patch = "PATCH";
    public final static String delete = "DELETE";

    public static String generateJSON(Map<String, String> parameters){

        String json = "";

        try {
            JSONObject jsonObject = new JSONObject();

            for (Map.Entry<String, String> entry : parameters.entrySet())
            {
                jsonObject.put(entry.getKey(), entry.getValue());
            }

            json = jsonObject.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return json;
    }

    private static URL createURL(String urlString){

        URL url = null;

        try {
            url = new URL(urlString);
        }catch (MalformedURLException m){
            //TODO tratar isso
            m.printStackTrace();
        }

        return url;
    }

    public static String[] post(String urlString, String body) throws IOException{
        return post(urlString, body, null);
    }

    public static String[] post(String urlString, String body, String token) throws IOException {
        return http(NetworkUtils.post, urlString, body, token);
    }

    public static String[] get(String urlString, String token) throws IOException {
        return http(NetworkUtils.get, urlString, null, token);
    }

    public static String[] put(String urlString, String body, String token) throws IOException {
        return http(NetworkUtils.put, urlString, body, token);
    }

    public static String[] delete(String urlString, String token) throws IOException {
        return http(NetworkUtils.delete, urlString, null, token);
    }

    private static String[] http(String method, String urlString, String body, String token) throws IOException {

        HttpURLConnection httpURLConnection = (HttpURLConnection) createURL(urlString).openConnection();

        httpURLConnection.setDoInput(true);

        setRequestHeader(httpURLConnection, method, "application/json");
        if (token != null) {
            httpURLConnection.setRequestProperty("Authorization", "JWT " + token);
        }

        if (body != null){
            httpURLConnection.setDoOutput(true);
            setRequestBody(httpURLConnection, body);
        }

        httpURLConnection.connect();

        return getResponse(httpURLConnection);
    }


    private static void setRequestBody(HttpURLConnection httpURLConnection, String parameter) {

        try {
            OutputStream os = httpURLConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(parameter);
            writer.flush();
            writer.close();
            os.close();
        }catch (IOException e){
            //TODO tratar exceção
        }
    }

    private static void setRequestHeader(HttpURLConnection httpURLConnection, String method, String contentType){

        if (!contentType.isEmpty()){
            httpURLConnection.setRequestProperty("Content-Type", contentType);
        }

        try {
            httpURLConnection.setRequestMethod(method);
        } catch (ProtocolException e) {
            e.printStackTrace();
        }

    }

    private static String[] getResponse(HttpURLConnection httpURLConnection) throws IOException{

        int statusCode = httpURLConnection.getResponseCode();

        InputStream in;
        if (statusCode >= 200 && statusCode < 400) {
            in = httpURLConnection.getInputStream();
        }
        else {
            in = httpURLConnection.getErrorStream();
        }

        //System.out.println("Response Code : " + statusCode);

        String[] response = new String[2];

        response[0] = Integer.toString(statusCode);
        response[1] = convertStreamToString(in);

        //System.out.println("Response : " + response[1]);

        return response;
    }
    private static String convertStreamToString(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}
