package findfood.a2.findfood.classes;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import findfood.a2.findfood.R;

import static android.content.Context.*;

/**
 * Created by amanda on 14/07/17.
 */

public class Login {

    private String token;

    public String login(String urlLogin, String email, String password) {

        Map<String, String> loginParameters = new HashMap<>();
        loginParameters.put("email", email);
        loginParameters.put("password", password);

        String json = NetworkUtils.generateJSON(loginParameters);

        String[] response = null;
        try {
            response = NetworkUtils.post(urlLogin, json);

        } catch (IOException e1) {
            e1.printStackTrace();
        }

        if (response == null)
            return null;

        int responseCode = Integer.parseInt(response[0]);
        if (responseCode >= 200 && responseCode <= 299){
            getTokenFromResponse(response[1]);
        }else{
            return null;
        }

        return getToken();
    }


    private void getTokenFromResponse(String response) {

        if (response != null){
            try {
                setToken(stringToJson(response).getString("token"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private static JSONObject stringToJson(String str) {
        JSONObject jsonObj = null;

        try {
            jsonObj = new JSONObject(str);
        }
        catch(JSONException e){
            e.getStackTrace();
        }

        return jsonObj;
    }

    private void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return this.token;
    }
}
