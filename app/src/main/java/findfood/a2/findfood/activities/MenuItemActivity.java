package findfood.a2.findfood.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import findfood.a2.findfood.R;
import findfood.a2.findfood.classes.MenuItem;
import findfood.a2.findfood.classes.NetworkUtils;
import findfood.a2.findfood.classes.Restaurant;

/**
 * Created by aluizio on 12/09/17.
 */

public class MenuItemActivity extends FragmentActivity implements OnMapReadyCallback {

    private MenuItem menuitem;
    private String token;

    private ScrollView menuItemScrollView;

    private TextView nameTextView;
    private TextView categoryTextView;
    private TextView descriptionTextView;

    private CheckBox likeButton;
    private CheckBox dislikeButton;

    private GoogleMap mMap;

    public static final String MENUITEMURL="MenuItemURL";
    public static final String TOKEN="TOKEN";
    public static final String IS_TO_CLOSE="IS_TO_CLOSE";

    private boolean isToClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menuitem_detail);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        String menuitemUrl = getIntent().getStringExtra(MENUITEMURL);
        token = getIntent().getStringExtra(TOKEN);
        isToClose = getIntent().getBooleanExtra(IS_TO_CLOSE, false);

        nameTextView = (TextView) findViewById(R.id.menu_item_name);
        categoryTextView = (TextView) findViewById(R.id.menu_item_category);
        descriptionTextView = (TextView) findViewById(R.id.menu_item_description);

        setScrollControll();

        likeButton = (CheckBox) findViewById(R.id.like_button);
        dislikeButton = (CheckBox) findViewById(R.id.dislike_button);
        menuitem = null;

        performMenuItem(menuitemUrl);
    }

    private void setScrollControll(){
        menuItemScrollView = (ScrollView) findViewById(R.id.scrollView_menu_item);
        ImageView transparent = (ImageView)findViewById(R.id.imagetrans);

        transparent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        menuItemScrollView.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        menuItemScrollView.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        menuItemScrollView.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    public void setLocationOnMap(final Restaurant restaurant){
        LatLng point = new LatLng(restaurant.getLatitude(), restaurant.getLongitude());
        MarkerOptions marker = new MarkerOptions().position(point).title(restaurant.getName());
        mMap.addMarker(marker);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 13.0f));
    }

    public void onClickRestaurant(View view){
        if(menuitem != null && !isToClose){
            String url = menuitem.getRestaurant().getRestaurantUrl();
            Intent intent = new Intent(getApplicationContext(), RestaurantActivity.class );
            intent.putExtra(RestaurantActivity.RESTAURANTURL, url);
            intent.putExtra(RestaurantActivity.TOKEN, token);
            startActivity(intent);
        }
        else{
            finish();
        }
    }

    private void loadLike(){
        likeButton.setChecked(menuitem.isMylike());
        dislikeButton.setChecked(menuitem.isMydislike());
    }

    public void onClickLike(View view){
        if(menuitem.isMylike()){
            likeButton.setChecked(false);
            menuitem.setMylike(false);
        }
        else{
            menuitem.setMylike(true);
            menuitem.setMydislike(false);
            dislikeButton.setChecked(false);
        }
        performLike();
    }

    public void onClickDislike(View view){
        if(menuitem.isMydislike()){
            dislikeButton.setChecked(false);
            menuitem.setMydislike(false);
        }
        else{
            menuitem.setMydislike(true);
            menuitem.setMylike(false);
            likeButton.setChecked(false);
        }
        performLike();
    }

    private void performMenuItem(String url){
        LoadData loadData = new LoadData();
        loadData.execute(url, token);
    }

    private void performLike(){
        try {
            String data = menuitem.getMylikeJSON();
            String url = menuitem.getLikeURL();
            LikeHandle likeHandle = new LikeHandle();
            likeHandle.execute(url, data, token);
        }
        catch(JSONException e){
            e.printStackTrace();
        }
    }

    private void loadMenuItem(){
        nameTextView.setText(menuitem.getName());
        categoryTextView.setText(menuitem.getCategory());
        descriptionTextView.setText(menuitem.getDescription());
        loadLike();
        Restaurant restaurant = menuitem.getRestaurant();
        if(restaurant != null){
            setLocationOnMap(restaurant);
        }
    }

    public void openLogin(){
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private class LoadData extends AsyncTask<String, String, String[]>{

        @Override
        protected String[] doInBackground(String... params){
            String[] response = new String[2];

            try {
                response = NetworkUtils.get(params[0], params[1]);

                if (response[0] != null){

                    int responseCode = Integer.parseInt(response[0]);

                    if (responseCode >= 200 && responseCode <= 299){

                        if(response[1] != null) {
                            JSONObject obj = new JSONObject(response[1]);
                            menuitem = new MenuItem(obj);
                        }

                    }
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String[] response) {
            super.onPostExecute(response);
            if(response[0] != null) {
                if (Integer.parseInt(response[0]) == 401) {
                    openLogin();
                }
                else{
                    loadMenuItem();
                }
            }
        }
    }

    private class LikeHandle extends AsyncTask<String, String, String[]>{
        @Override
        protected String[] doInBackground(String... params){

            String[] response = new String[2];

            try {
                response = NetworkUtils.put(params[0], params[1], params[2]);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }

        protected void onPostExecute(String[] response) {
            super.onPostExecute(response);
            if(response[0] != null) {
                if (Integer.parseInt(response[0]) == 401) {
                    openLogin();
                }
            }
        }
    }
}
