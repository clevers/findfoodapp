package findfood.a2.findfood.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import findfood.a2.findfood.R;
import findfood.a2.findfood.classes.Login;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText usernameEditText;
    private EditText passwordEditText;

    private Button loginButton;
    private Button createAccountButton;

    private RelativeLayout loadingLogin;

    private TextView textView;

    public static final String TOKEN="TOKEN";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        usernameEditText = (EditText)findViewById(R.id.input_userName);
        passwordEditText = (EditText)findViewById(R.id.input_password);

        loadingLogin = (RelativeLayout)findViewById(R.id.loading_login);

        loginButton = (Button) findViewById(R.id.button_login);
        loginButton.setOnClickListener(this);
        createAccountButton = (Button) findViewById(R.id.button_create_account);
        createAccountButton.setOnClickListener(this);

        textView = (TextView) findViewById(R.id.textView);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_login:

                String username = usernameEditText.getText().toString();
                String password = passwordEditText.getText().toString();

//                if(username.compareTo("")==0 && password.compareTo("")==0) {
//                    username = "amanda@gmail.com";
//                    password = "amanda";
//                }

                new DoLogin().execute(username, password);
                break;

            case R.id.button_create_account:

                Intent intent = new Intent(getApplicationContext(), CreateAccountActivity.class );
                startActivity(intent);
        }
    }

    private class DoLogin extends AsyncTask<String, String, String[]> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            textView.setVisibility(View.GONE);
            loadingLogin.setVisibility(View.VISIBLE);
        }

        @Override
        protected String[] doInBackground(String... params) {

            if (params.length == 0) {
                return null;
            }

            String username = params[0];
            String password = params[1];

            String urlLogin =  getResources().getString(R.string.login_url);

            Login loginObject = new Login();

            String token = loginObject.login(urlLogin, username, password);

            if (token != null){
                Intent intent = new Intent(getApplicationContext(), MainActivity.class );
                intent.putExtra(MainActivity.TOKEN, token);
                startActivity(intent);
            }

            String[] response = new String[1];
            response[0] = token;
            return response;
        }

        @Override
        protected void onPostExecute(String[] response) {
            loadingLogin.setVisibility(View.GONE);
            textView.setVisibility(View.VISIBLE);

            textView.setText("");

            if (response[0] == null){
                textView.setTextColor(Color.parseColor("#A73E47"));
                textView.setText(R.string.login_error);
            }

        }
    }
}
