package findfood.a2.findfood.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import findfood.a2.findfood.R;
import findfood.a2.findfood.classes.NetworkUtils;

public class CreateAccountActivity extends AppCompatActivity {


    private EditText emailEditText;
    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private EditText passwordEditText;
    private EditText confirmPasswordEditText;

    private Button createAccountButton;

    private RelativeLayout loadingCreateAccount;

    private TextView messageTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        firstNameEditText = (EditText)findViewById(R.id.cr_input_first_name);
        lastNameEditText = (EditText)findViewById(R.id.cr_input_last_name);
        emailEditText = (EditText)findViewById(R.id.cr_input_email);
        passwordEditText = (EditText)findViewById(R.id.cr_input_password);
        confirmPasswordEditText = (EditText)findViewById(R.id.cr_input_password_confirm);

        messageTextView = (TextView) findViewById(R.id.tv_message_account);

        loadingCreateAccount = (RelativeLayout) findViewById(R.id.loading_create_account);

        createAccountButton = (Button) findViewById(R.id.button_create_account);
        createAccountButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                handleButtonClick(createAccountButton);
            }
        });
    }

    private void handleButtonClick(Button button){

        if (button.getId() == R.id.button_create_account){

            String firstName = firstNameEditText.getText().toString();
            String lastName = lastNameEditText.getText().toString();
            String email = emailEditText.getText().toString();
            String password = passwordEditText.getText().toString();
            String passwordConfirm = confirmPasswordEditText.getText().toString();

            if (isFormOK(firstName, lastName, email, password, passwordConfirm)) {
                messageTextView.setText("");
                new CreateAccount().execute(firstName, lastName, email, password);
            }
        }
    }

    private boolean isFormOK(String firstName, String lastName, String email, String password, String passwordConfirm){

        if (email.trim().isEmpty() || firstName.trim().isEmpty() || lastName.trim().isEmpty()){
            giveMessage("Preencha todos os campos, por favor");
            return false;
        }

        return isEmailOK(email) && isPasswordOk(password, passwordConfirm);

    }

    private boolean isEmailOK(String email){

        //Padrão pego nesse site http://emailregex.com/
        String pattern = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

        Pattern r = Pattern.compile(pattern);

        Matcher m = r.matcher(email);

        if (!m.find()){
            giveMessage("Digite um e-mail válido, por favor");
            return false;
        }

        return true;
    }


    private boolean isPasswordOk(String password, String passwordConfirm){

        String message = "";

        if (password.length() < 5){
            message = "A senha deve ter pelo menos 5 caracteres";
        }else if (password.trim().isEmpty()){
            message = "Sua senha deve ser composta por letras, números e caracteres especiais";
        }else if (!password.equals(passwordConfirm)){
            message = "As senhas não conferem";
        }

        if (message.isEmpty())
            return true;

        giveMessage(message);

        return false;

    }

    private void giveMessage(String message){
        messageTextView.setText(message);
    }

    private class CreateAccount extends AsyncTask<String, String, String[]> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingCreateAccount.setVisibility(View.VISIBLE);
            messageTextView.setVisibility(View.GONE);
        }

        @Override
        protected String[] doInBackground(String... params) {

            if (params.length == 0) {
                return null;
            }

            Map<String, String> createAccountParameters = new HashMap<>();
            createAccountParameters.put("first_name", params[0]);
            createAccountParameters.put("last_name", params[1]);
            createAccountParameters.put("email", params[2]);
            createAccountParameters.put("password", params[3]);

            String url = getResources().getString(R.string.user);
            String json = NetworkUtils.generateJSON(createAccountParameters);

            String[] response = new String[1];

            try {
                response = NetworkUtils.post(url, json);
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            return response;
        }

        private String createMessageFromResponse(String[] response) {
            int statusCode = Integer.parseInt(response[0]);

            if (statusCode >= 500){
                return "Erro no servidor, tente novamente mais tarde";
            }else {
                if (statusCode == 400) {
                    if (response[1].contains("user with this email address already exists")) {
                        return "Já existe um usuário com esse e-mail";
                    }
                } else {
                    if (statusCode >= 200 && statusCode <= 299) {
                        int color = ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark);
                        messageTextView.setTextColor(color);
                        return ("Sua conta foi criada com sucesso!");
                    }
                }
            }

            return response[1];
        }


        @Override
        protected void onPostExecute(String[] responses) {
            loadingCreateAccount.setVisibility(View.GONE);
            messageTextView.setVisibility(View.VISIBLE);

            int TIME_OUT = 1500; //Time to launch the another activity

            messageTextView.setText("");

            if (responses != null) {

                int statusCode = Integer.parseInt(responses[0]);


                messageTextView.append(createMessageFromResponse(responses) + "\n\n\n");


                if (statusCode >= 200 && statusCode <= 299){

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class );
                            startActivity(intent);
                            finish();
                        }
                    }, TIME_OUT);

                }

            }
        }


    }
}
