package findfood.a2.findfood.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import findfood.a2.findfood.R;
import findfood.a2.findfood.classes.EndlessScrollListener;
import findfood.a2.findfood.classes.MenuItem;
import findfood.a2.findfood.classes.MenuItemAdapter;
import findfood.a2.findfood.classes.MenuItemList;
import findfood.a2.findfood.classes.NetworkUtils;
import findfood.a2.findfood.classes.RecommendationAdapter;
import findfood.a2.findfood.classes.RecommendationList;

public class MainActivity extends AppCompatActivity {

    private EditText searchText;
    private RecyclerView menuItemListView;
    private RecyclerView recommendationsView;

    private RelativeLayout loadingView;
    private RelativeLayout loadingRecommendationsView;

    private FrameLayout frameLoadingRecommendation;
    private LinearLayout recommendationLayout;

    private MenuItemList menuItemList;
    private RecommendationList recommendations;

    private TextView noRecommendationText;
    private TextView noMenuItemText;

    private String latitude;
    private String longitude;
    private LocationManager locationManager;
    private String token;

    private final static int MENU_SERVICES_LOCATION = 200;

    public static final String TOKEN="TOKEN";

    private static final int TIME_OUT=5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        searchText = (EditText) findViewById(R.id.search_edit_text);

        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch(searchText);
                    return true;
                }
                return false;
            }
        });

        menuItemListView = (RecyclerView) findViewById(R.id.menu_item_recycler);
        recommendationsView = (RecyclerView) findViewById(R.id.menu_item_recycler_recommendation);
        frameLoadingRecommendation = (FrameLayout) findViewById(R.id.frameLoadingRecommendation);

        recommendationLayout = (LinearLayout) findViewById(R.id.recommendation_layout);

        noRecommendationText = (TextView) findViewById(R.id.no_recommendation_text_view);
        noMenuItemText = (TextView) findViewById(R.id.items_not_found);

        loadingView = (RelativeLayout) findViewById(R.id.loading);
        loadingRecommendationsView = (RelativeLayout) findViewById(R.id.loadingRecommendations);

        LinearLayoutManager menuItemListLayout = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        LinearLayoutManager recommendationsLayout = new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false);

        menuItemListView.setLayoutManager(menuItemListLayout);

        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(
                menuItemListView.getContext(),
                menuItemListLayout.getOrientation()
        );

        menuItemListView.addItemDecoration(mDividerItemDecoration);

        recommendationsView.setLayoutManager(recommendationsLayout);

        menuItemListView.addOnScrollListener(new EndlessScrollListener(menuItemListLayout) {
            @Override
            public void onLoadMore(int page) {
                loadNextDataFromMenuItemList(page);
            }
        });
        recommendationsView.addOnScrollListener(new EndlessScrollListener(recommendationsLayout) {
            @Override
            public void onLoadMore(int page) {
                loadNextDataFromRecommendations(page);
            }
        });

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 50, 1000, new MenuLocationListener());
        }

        latitude = null;
        longitude = null;

        token = getIntent().getStringExtra(TOKEN);
        performRecommendation();

        askToActivateLocationService();
    }

    private boolean hasLocationPermission(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults){
        switch (requestCode){
            case MENU_SERVICES_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 50, 1000, new MenuLocationListener());
                }
                else{
                    locationManager = null;
                    latitude = null;
                    longitude = null;
                }
                break;
            default:
                break;

        }
    }

    private void askToActivateLocationService(){
        if (hasLocationPermission() && locationManager != null && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            return;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final MainActivity thisActivity = this;

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (!hasLocationPermission()) {
                    ActivityCompat.requestPermissions(thisActivity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MENU_SERVICES_LOCATION);
                }
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 1);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });

        builder.setTitle(R.string.dialog_title);
        builder.setMessage(R.string.dialog_message);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onResume(){
        super.onResume();
        performRecommendation(true);
    }

    public boolean isRecommendationHidden(){
        return recommendationLayout.getVisibility() == View.GONE;
    }

    public void hideRecommendationFrameLayout(ImageButton imageButton){
        recommendationLayout.setVisibility(View.GONE);
        imageButton.setImageResource(R.mipmap.ic_keyboard_arrow_up_white_36dp);
    }

    public void showRecommendationFrameLayout(ImageButton imageButton){
        recommendationLayout.setVisibility(View.VISIBLE);
        imageButton.setImageResource(R.mipmap.ic_keyboard_arrow_down_white_36dp);
    }

    public void onClickHide(View view){

        ImageButton imageButton = (ImageButton) view;

        if (view.getId() == R.id.hide_recommendation_image_button) {

            if (isRecommendationHidden()) {
                showRecommendationFrameLayout(imageButton);
            }else{
                hideRecommendationFrameLayout(imageButton);
            }
        }
    }

    private class MenuLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location loc) {
            latitude = ""+loc.getLatitude();
            longitude = ""+loc.getLongitude();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}

        @Override
        public void onProviderEnabled(String provider) {
            latitude = null;
            longitude = null;
        }

        @Override
        public void onProviderDisabled(String provider) {
            latitude = null;
            longitude = null;
        }
    }

    public void openMenuItemDetail(MenuItem menuItem){
        Intent intent = new Intent(this, MenuItemActivity.class);
        intent.putExtra(MenuItemActivity.MENUITEMURL, menuItem.getMenuUrl());
        intent.putExtra(MenuItemActivity.TOKEN, token);
        startActivity(intent);
    }

    public void openLogin(){
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void onClickMenuItem(View view){
        int itemPosition = menuItemListView.getChildLayoutPosition(view);
        if(menuItemList != null && menuItemList.size() > 0) {
            MenuItem menuItem = menuItemList.getMenuItems().get(itemPosition);
            openMenuItemDetail(menuItem);
        }
    }

    public void onClickRecommendation(View view){
        int itemPosition = recommendationsView.getChildLayoutPosition(view);
        if(recommendations != null && recommendations.size() > 0) {
            MenuItem menuItem = recommendations.getRecommendations().get(itemPosition).getMenuItem();
            openMenuItemDetail(menuItem);
        }
    }

    protected void loadNextDataFromMenuItemList(int page){
        String url = menuItemList.getNextUrl();
        if(url != null){
            Search search = new Search();
            search.execute(url, token);
            performRecommendation();
        }
    }

    protected void loadNextDataFromRecommendations(int page){
        String url = recommendations.getNextUrl();
        if(url != null){
            Recommendation recommendation = new Recommendation();
            recommendation.execute(url, token);
        }
    }

    private void performSearch(EditText searchEditText) {

        String queryString = searchEditText.getText().toString();

        if(queryString != null) {
            menuItemList = null;
            String url;
            if(latitude != null && longitude != null){
                url = String.format(getResources().getString(R.string.menu_location_url), latitude, longitude, "5");
            }
            else{
                url = getResources().getString(R.string.menu_url);
            }
            url += "?q=" + queryString;
            Search search = new Search();

            noMenuItemText.setVisibility(View.GONE);
            search.execute(url, token);
        }
    }

    private void performRecommendation(){
        recommendations = null;
        String url = getResources().getString(R.string.recommendations_url);
        Recommendation recommendation = new Recommendation();
        recommendation.execute(url, token);
    }

    private void performRecommendation(boolean isTowait){
        if(isTowait) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    performRecommendation();
                }
            }, TIME_OUT);
        }
        else{
            performRecommendation();
        }
    }

    private void loadMenuItems(boolean isToAddAdapter) {
        if(menuItemList == null) return;

        if(menuItemList.size() > 0) {
            noMenuItemText.setVisibility(View.GONE);
        }
        else {
            noMenuItemText.setVisibility(View.VISIBLE);
        }

        if (isToAddAdapter){
            MenuItemAdapter adapter = new MenuItemAdapter(menuItemList, this);
            menuItemListView.setAdapter(adapter);
            performRecommendation(true);
        }
        else{
            MenuItemAdapter adapter = (MenuItemAdapter) menuItemListView.getAdapter();
            if(adapter != null)
                adapter.notifyDataSetChanged();
        }
    }

    private void loadRecommendations(boolean isToAddAdapter) {
        if(recommendations == null) return;

        if(recommendations.size() > 0) {
            noRecommendationText.setVisibility(View.GONE);
        }
        else {
            noRecommendationText.setVisibility(View.VISIBLE);
        }

        if (isToAddAdapter){
            RecommendationAdapter adapter = new RecommendationAdapter(recommendations, this);
            recommendationsView.setAdapter(adapter);
        }
        else{
            RecommendationAdapter adapter = (RecommendationAdapter) recommendationsView.getAdapter();
            adapter.notifyDataSetChanged();
        }
    }

    private class Search extends AsyncTask<String, String, String[]> {

        private boolean isToAddAdapter;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingView.setVisibility(View.VISIBLE);
        }

        @Override
        protected String[] doInBackground(String... params) {

            String[] response = new String[2];

            try {
                response = NetworkUtils.get(params[0], params[1]);

                if (response[0] != null){

                    int responseCode = Integer.parseInt(response[0]);

                    if (responseCode >= 200 && responseCode <= 299){

                        if(response[1] != null) {
                            JSONObject obj = new JSONObject(response[1]);
                            if(menuItemList == null) {
                                isToAddAdapter = true;
                                menuItemList = new MenuItemList(obj);
                            }
                            else{
                                menuItemList.updateList(obj);
                                isToAddAdapter = false;
                            }
                        }

                    }
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String[] response) {
            super.onPostExecute(response);
            loadingView.setVisibility(View.GONE);

            if(response[0] != null) {
                if (Integer.parseInt(response[0]) == 401) {
                    openLogin();
                }
                else{
                    loadMenuItems(isToAddAdapter);
                }
            }
        }


    }

    private class Recommendation extends AsyncTask<String, String, String[]> {

        private boolean isToAddAdapter;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingRecommendationsView.setVisibility(View.VISIBLE);
        }

        @Override
        protected String[] doInBackground(String... params) {
            String[] response = new String[2];
            try {
                response = NetworkUtils.get(params[0], params[1]);

                if (response[0] != null){

                    int responseCode = Integer.parseInt(response[0]);

                    if (responseCode >= 200 && responseCode <= 299){

                        if(response[1] != null) {
                            JSONObject obj = new JSONObject(response[1]);
                            if(recommendations == null) {
                                isToAddAdapter = true;
                                recommendations = new RecommendationList(obj);
                            }
                            else{
                                recommendations.updateList(obj);
                                isToAddAdapter = false;
                            }
                        }

                    }
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String[] response) {
            super.onPostExecute(response);
            loadingRecommendationsView.setVisibility(View.GONE);

            if(response[0] != null) {
                int responseCode = Integer.parseInt(response[0]);
                if(responseCode == 401){
                    openLogin();
                }
                else {
                    if (responseCode >= 200 && responseCode <= 299) {
                        loadRecommendations(isToAddAdapter);
                    }
                }
            }
        }
    }
}
