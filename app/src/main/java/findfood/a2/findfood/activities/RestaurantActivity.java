package findfood.a2.findfood.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import findfood.a2.findfood.R;
import findfood.a2.findfood.classes.EndlessScrollListener;
import findfood.a2.findfood.classes.MenuItem;
import findfood.a2.findfood.classes.MenuItemAdapter;
import findfood.a2.findfood.classes.MenuItemList;
import findfood.a2.findfood.classes.NetworkUtils;
import findfood.a2.findfood.classes.Restaurant;

public class RestaurantActivity extends AppCompatActivity {

    private TextView nameView;
    private TextView descriptionView;
    private RecyclerView menuitemsView;

    private RelativeLayout loadingView;

    private Restaurant restaurant;
    private MenuItemList menuItems;

    private String token;

    public static final String RESTAURANTURL="RESTAURANT_URL";
    public static final String TOKEN="TOKEN";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);

        nameView = (TextView) findViewById(R.id.restaurant_name);
        descriptionView = (TextView) findViewById(R.id.restaurant_description);
        menuitemsView = (RecyclerView) findViewById(R.id.menu_item_restaurants);

        loadingView = (RelativeLayout) findViewById(R.id.loading);

        LinearLayoutManager menuLayout = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);

        menuitemsView.setLayoutManager(menuLayout);

        menuitemsView.addOnScrollListener(new EndlessScrollListener(menuLayout) {
            @Override
            public void onLoadMore(int page) {
                loadNextDataFromMenu(page);
            }
        });

        String restaurantUrl = getIntent().getStringExtra(RESTAURANTURL);
        token = getIntent().getStringExtra(TOKEN);

        performData(restaurantUrl, "R");
    }

    public void openMenuItemDetail(MenuItem menuItem){
        Intent intent = new Intent(this, MenuItemActivity.class);
        intent.putExtra(MenuItemActivity.MENUITEMURL, menuItem.getMenuUrl());
        intent.putExtra(MenuItemActivity.TOKEN, token);
        intent.putExtra(MenuItemActivity.IS_TO_CLOSE, true);
        startActivity(intent);
        //finish();
    }

    public void onClickMenuItem(View view){
        int itemPosition = menuitemsView.getChildLayoutPosition(view);
        MenuItem menuItem = menuItems.getMenuItems().get(itemPosition);
        openMenuItemDetail(menuItem);
    }


    private void performData(String url, String type){
        LoadData loadData = new LoadData();
        loadData.execute(url, token, type);
    }

    private void loadNextDataFromMenu(int page){
        String url = menuItems.getNextUrl();
        if(url!=null){
            performData(url, "M");
        }
    }

    private void loadData(boolean isToAddAdapter){
        if(restaurant != null && menuItems == null){
            nameView.setText(restaurant.getName());
            descriptionView.setText(restaurant.getDescription());
            performData(restaurant.getMenu(), "M");
        }
        else if(menuItems != null){
            if(isToAddAdapter) {
                MenuItemAdapter adapter = new MenuItemAdapter(menuItems, this, false);
                menuitemsView.setAdapter(adapter);
            }
            else{
                MenuItemAdapter adapter = (MenuItemAdapter) menuitemsView.getAdapter();
                adapter.notifyDataSetChanged();
            }
        }
    }

    public void openLogin(){
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    private class LoadData extends AsyncTask<String, String, String[]> {
        private boolean isToAddAdapter;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingView.setVisibility(View.VISIBLE);
        }

        @Override
        protected String[] doInBackground(String... params){
            String[] response = new String[2];

            try {
                response = NetworkUtils.get(params[0], params[1]);

                if (response[0] != null){

                    int responseCode = Integer.parseInt(response[0]);

                    if (responseCode >= 200 && responseCode <= 299){

                        if(response[1] != null) {
                            JSONObject obj = new JSONObject(response[1]);
                            switch (params[2]){
                                case "R":
                                    restaurant = new Restaurant(obj);
                                    break;
                                case "M":
                                    if(menuItems == null) {
                                        isToAddAdapter = true;
                                        menuItems = new MenuItemList(obj);
                                    }
                                    else{
                                        menuItems.updateList(obj);
                                        isToAddAdapter = false;
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }

                    }
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String[] response) {
            super.onPostExecute(response);
            loadingView.setVisibility(View.GONE);
            if(response[0] != null) {
                if (Integer.parseInt(response[0]) == 401) {
                    openLogin();
                }
                else{
                    loadData(isToAddAdapter);
                }
            }

        }
    }

}
